gulp = require 'gulp'
del = require 'del'
runSequence = require 'run-sequence'


dirs =
  t3: 'custom_modules/threejs/'


# ===== copy =====

gulp.task 'copy:threejs', ->
  gulp.src [
    dirs.t3 + 'build/*.js'
    dirs.t3 + 'examples/js/**/*.js'
  ]
    .pipe gulp.dest 'public/libs/three'


gulp.task 'copy:libs', [ 'copy:threejs' ]


# ===== setup =====

gulp.task 'clean', (next) ->
  del [
    'public/libs'
    'public/drafts/*/main.js'
  ], next


gulp.task 'default', ->
  runSequence 'clean', 'copy:libs'
