var path = require('path'),
    webpack = require('webpack')

module.exports = {

    entry: {
        // test: ['./src/drafts/test/main.coffee'],
        // canvas_test: ['./src/drafts/canvas_test/main.coffee'],
        // events_t3_test: ['./src/drafts/events_t3_test/main.coffee'],
        // render_test: ['./src/drafts/render_test/main.coffee'],
        // render_animation_test: ['./src/drafts/render_animation_test/main.coffee'],
        render_collection_test: ['./src/drafts/render_collection_test/main.js'],
        benchmarks: ['./src/drafts/benchmarks/main.js']
    },

    output: {
        path: './public',
        filename: 'drafts/[name]/main.js',
        publicPath: '/',
        hotUpdateMainFilename: '[hash].hot-update.json'
    },

    debug: true,

    module: {
        loaders: [
            {
                loaders: ['react-hot', 'babel'],
                test: /\.jsx?$/,
                include: [
                    path.join(__dirname, 'src'),
                    path.join(__dirname, 'custom_modules', 'tslibs', 'src'),
                    path.join(__dirname, 'custom_modules', 'tvs-renderer', 'lib'),
                    path.join(__dirname, 'custom_modules', 'tvs-entity-system', 'lib'),
                    path.join(__dirname, 'custom_modules', 'tvs-entity-system', 'benchmarks'),
                    path.join(__dirname, 'custom_modules', 'tvs-entity-editor', 'lib'),
                    path.join(__dirname, 'node_modules', 'vis', 'lib'),
                ]
            }, {
                loader: 'coffee-loader',
                test: /\.coffee$/
            }, {
                loader: 'style!raw!stylus',
                test: /\.styl$/
            }, {
                loader: 'style!raw',
                test: /\.css$/
            }
        ]
    },

    resolve: {
        extensions: ['', '.js', '.jsx', '.json', '.coffee'],
        root: [
            path.resolve(__dirname, "./src"),
            path.resolve(__dirname, "./custom_modules"),
            path.resolve(__dirname, "./node_modules")
        ],

        alias: {
            ts: 'tslibs/src'
        }
    },

    devtool: '#cheap-module-eval-source-map',
    // devtool: '#inline-source-map',

    devServer: {
        contentBase: "./public",
        publicPath: '/',
        noInfo: false,
        quiet: true,
        hot: true,
        inline: true,
        colors: true
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
}
