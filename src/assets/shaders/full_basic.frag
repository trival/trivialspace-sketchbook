#if MAX_DIR_LIGHTS > 0
	uniform vec3 directionalLightColor[ MAX_DIR_LIGHTS ];
	varying vec3 vDirLight[ MAX_DIR_LIGHTS ];
#endif

#if MAX_POINT_LIGHTS > 0
	uniform vec3 pointLightColor[ MAX_POINT_LIGHTS ];
	varying vec4 vPointLight[ MAX_POINT_LIGHTS ];
#endif

uniform vec3 ambientLightColor;

varying vec3 vNormal;

void main(void)
{
    vec3 eyedir = vec3(0.0,0.0,1.0);
    vec4 color = vec4(vec3(0.5),1.0);
    float uShininess = 30.0;
    
    #if MAX_POINT_LIGHTS > 0

		vec4 pointTotal  = vec4( 0.0 );
		for ( int i = 0; i < MAX_POINT_LIGHTS; i ++ ) {

			vec3 pointVector = normalize( vPointLight[ i ].xyz );
			vec3 pointHalfVector = normalize( pointVector + eyedir );
			float pointDistance = vPointLight[ i ].w;

			float pointDotNormalHalf = dot( vNormal, pointHalfVector );
			float pointDiffuseWeight = max( dot( vNormal, pointVector ), 0.0 );

			float pointSpecularWeight = 0.0;
			if ( pointDotNormalHalf >= 0.0 )
				pointSpecularWeight = pow( pointDotNormalHalf, uShininess );

			pointTotal  += pointDistance * vec4( pointLightColor[ i ], 1.0 ) * ( color * pointDiffuseWeight + color * pointSpecularWeight * pointDiffuseWeight );

		}
	#endif

	#if MAX_DIR_LIGHTS > 0

		vec4 dirTotal  = vec4( 0.0 );

		for( int i = 0; i < MAX_DIR_LIGHTS; i++ ) {

			vec3 dirVector = normalize( vDirLight[i] );
			vec3 dirHalfVector = normalize( dirVector + eyedir );

			float dirDotNormalHalf = dot( vNormal, dirHalfVector );
			float dirDiffuseWeight = max( dot( vNormal, dirVector ), 0.0 );

			float dirSpecularWeight = 0.0;
			if ( dirDotNormalHalf >= 0.0 )
				dirSpecularWeight = pow( dirDotNormalHalf, uShininess );

			dirTotal  += vec4( directionalLightColor[ i ], 1.0 ) * ( color * dirDiffuseWeight + color * dirSpecularWeight * dirDiffuseWeight );

		}
	#endif

	// all lights contribution summation

	vec4 totalLight = vec4( ambientLightColor * vec3(0.2), 1.0 );

	#if MAX_DIR_LIGHTS > 0
		totalLight += dirTotal;
	#endif

	#if MAX_POINT_LIGHTS > 0
		totalLight += pointTotal;
	#endif

	gl_FragColor = totalLight;
}