keys = require 'ts/events/direction-keys'
mouse = require 'ts/events/mouse-press-element'
drag = require 'ts/events/drag-element'


values =

  'shader.vert': require '!raw!assets/shaders/basic.vert'
  'shader.frag': require '!raw!assets/shaders/basic.frag'

  'moveSpeed':
    1

  'cameraDirection':
    [0, 0, 0]

  'lookSpeed':
    0.1

  'cameraMaxRotX':
    85

  'cameraRot':
    y: 10
    x: 10

  'cameraConfig':
    fov: 75
    near: 1
    far: 10000

  'cameraPosition':
    x: 0
    y: 0
    z: 500

  'qubeSize':
    [100, 100, 100]


entities =

  'moveDir':
    init: ->
      forth: false
      back: false
      left: false
      right: false


    reactions:
      "#{keys.FORTH} #{mouse.RIGHT_BTN}":
        (dir, forth, btn) ->
          dir.forth = forth or btn
          return
      "#{keys.BACK} #{mouse.MIDDLE_BTN}":
        (dir, back, btn) ->
          dir.back = back or btn
          return
      "#{keys.RIGHT}":
        (dir, right) ->
          dir.right = right
          return
      "#{keys.LEFT}":
        (dir, left) ->
          dir.left = left
          return


  'cameraDirection':
    reactions:
      'moveDir tpf':
        require: 'cameraRotYMatrix'
        update: (dir, move, _, mat) ->
          es = mat.elements
          x = y = z = 0
          if move.forth
            x -= es[8]
            y -= es[9]
            z -= es[10]
          if move.back
            x += es[8]
            y += es[9]
            z += es[10]
          if move.left
            x -= es[0]
            y -= es[1]
            z -= es[2]
          if move.right
            x += es[0]
            y += es[1]
            z += es[2]

          if x is y is z is 0
            dir[0] = dir[1] = dir[2] = 0
            return
          else
            [x, y, z]


  'cameraPosition':
    reactions:
      'cameraDirection moveSpeed':
        (pos, [x, y, z], speed) ->
          # console.log 'calculating cameraPosition'
          pos.x += x * speed
          pos.y += y * speed
          pos.z += z * speed
          return


  'canvasSize':
    init: ->
      [window.innerWidth, window.innerHeight]


  'cameraRot':
    reactions:
      "#{drag.DELTA_X} lookSpeed":
        (rot, deltaX, speed) ->
          rot.y += deltaX * speed
          return
      "#{drag.DELTA_Y} lookSpeed":
        (rot, deltaY, speed) ->
          rot.x += deltaY * speed
          return


  'cameraRotXMatrix':
    init: ->
      new THREE.Matrix4()

    reactions:
      'cameraRot cameraMaxRotX':
        (mat, rot, max) ->
          if max > Math.abs rot.x
            rot = THREE.Math.degToRad rot.x
            mat.makeRotationFromEuler new THREE.Euler rot, 0, 0
            return


  'cameraRotYMatrix':
    init: ->
      new THREE.Matrix4()

    reactions:
      'cameraRot':
        (mat, rot) ->
          rot = THREE.Math.degToRad rot.y
          mat.makeRotationFromEuler new THREE.Euler 0, rot, 0
          return


  'renderer':
    init:  ->
      r = new THREE.WebGLRenderer()
      document.body.appendChild r.domElement
      r

    reactions:
      'canvasSize':
        (r, size) ->
          r.setSize size...
          return


  'scene':
    init: ->
      new THREE.Scene()


  'camera':
    require: 'cameraConfig canvasSize'
    init: (conf, [w, h]) ->
      c = new THREE.PerspectiveCamera conf.fov
        , w / h
        , conf.near
        , conf.far
      c.matrixAutoUpdate = false
      c

    reactions:
      'cameraPosition cameraRotXMatrix cameraRotYMatrix':
        (cam, pos, matX, matY) ->
          # console.log 'calculating camera'
          cam.matrix.multiplyMatrices matY, matX
          cam.matrix.setPosition pos
          cam.matrixWorldNeedsUpdate = true
          return


  'geometry':
    require: 'qubeSize'
    init: (size) ->
      new THREE.BoxGeometry size...


  'material':
    init: ->
      new THREE.ShaderMaterial()

    reactions:
      'shader.vert shader.frag':
        (mat, vert, frag) ->
          mat.vertexShader = vert
          mat.fragmentShader = frag
          mat.needsUpdate = true
          return


  'qube':
    require: 'geometry material scene'
    init: (geo, mat, scene) ->
      m = new THREE.Mesh geo, mat
      scene.add m
      m



actions =
  'render':
    require: 'renderer scene camera'
    update: (renderer, scene, cam) ->
      renderer.render scene, cam
      return


module.exports = {
  values,
  entities,
  actions
}
