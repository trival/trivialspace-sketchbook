ES = require 'ts/systems/entity-system'
Animation = require 'ts/utils/animation'
drag = require 'ts/events/drag-element'
mouse = require 'ts/events/mouse-press-element'
keys = require 'ts/events/direction-keys'
app = require './app'


sys = ES.create()

drag.init sys
mouse.init sys, document, enableRightButton: true
keys.init sys

ES.addValues sys, app.values
ES.addEntities sys, app.entities
# ES.addActions sys, app.actions

animator = Animation.animator (tpf) ->
  ES.set sys, 'tpf', tpf
  ES.flush sys
  renderer = ES.get sys, 'renderer'
  renderer.render ES.get(sys, 'scene'), ES.get sys, 'camera'

animator.start()

window.ES = ES
window.animator = animator
window.sys = sys
