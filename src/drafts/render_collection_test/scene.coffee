Oscillators = require 'tvs-entity-system/lib/utils/oscillators'
consts = require 'tvs-renderer/lib/constants'
assetLib = require 'tvs-renderer/lib/asset-lib'
{updateShader, updateGeometry, updateObject, updateLayer} =
  require 'tvs-renderer/lib/utils/entity-system-update-reactions'

console.log Oscillators

module.exports =

  # ===== Context setup =====

  'ctx':
    init: ->
      ctx = @Renderer.create()
      document.body.appendChild ctx.gl.canvas
      @Renderer.updateSize ctx


  # ===== Event signals =====

  'mousePress':
    oscillate: Oscillators.mousePress enableRightButton: true
    target: 'mouseBtns'


  'mouseDrag':
    oscillate: Oscillators.mouseDrag()
    target: 'mouseDragDelta'


  'directionKeys':
    oscillate: Oscillators.directionKeys()
    target: 'keyDirections'


  # ===== General settings =====

  'animator':
    oscillate:
      do: Oscillators.requestAnimation autoFlush: true
      require: 'startAnimate'
    target: 'tpf'
    reactions:
      'stopAnimate': (animator, stop) ->
        animator() if stop
        return


  'tpf':
    value: 0


  'settings/size':
    init:
      require: 'ctx'
      do: (ctx) ->
        w: ctx.settings.width
        h: ctx.settings.height


  # ===== Camera =====

  'moveSpeed':
    value: 0.1


  'lookSpeed':
    value: 0.01


  'moveDir':
    init: ->
      forth: false
      back: false
      left: false
      right: false


    reactions:
      "keyDirections mouseBtns":
        (dir, dirs, btns) ->
          dir.forth = dirs.forth or btns.rightButton
          dir.back = dirs.back or btns.middleButton
          dir.right = dirs.right
          dir.left = dirs.left
          return


  'camera/direction':
    value:
      [0, 0, 0]

    reactions:
      'moveDir tpf':
        require: 'camera/rotYMatrix'
        do: (dir, move, _, mat) ->
          x = y = z = 0
          if move.forth
            x -= mat[8]
            y -= mat[9]
            z -= mat[10]
          if move.back
            x += mat[8]
            y += mat[9]
            z += mat[10]
          if move.left
            x -= mat[0]
            y -= mat[1]
            z -= mat[2]
          if move.right
            x += mat[0]
            y += mat[1]
            z += mat[2]

          if x is y is z is 0
            dir[0] = dir[1] = dir[2] = 0
            return
          else
            [x, y, z]


  'camera/position':
    value:
      [0, 0, 50]

    reactions:
      'camera/direction moveSpeed':
        (pos, [x, y, z], speed) ->
          pos[0] += x * speed
          pos[1] += y * speed
          pos[2] += z * speed
          return


  'camera/maxRotX':
    value:
      Math.PI / 2 - 0.05


  'camera/rot':
    value:
      y: 0.1
      x: 0.1

    reactions:
      "mouseDragDelta lookSpeed camera/maxRotX":
        (rot, delta, speed, max) ->
          old = rot.x
          rot.y += delta.x * speed
          rot.x += delta.y * speed
          if max < Math.abs rot.x
            rot.x = old
          return


  'camera/rotYMatrix':
    init: ->
      @mat4.create()

    reactions:
      'camera/rot':
        (m, rot) ->
          @mat4.fromYRotation m, rot.y
          return


  'camera/properties':
    value:
      fovy: Math.PI / 3
      near: 0.1
      far: 1000

    reactions:
      'settings/size': (props, size) ->
        props.aspect = size.w / size.h
        return


  'camera':
    init: ->
      projection: @mat4.create()
      transform: @mat4.create()
    reactions:
      'camera/properties': (cam, props) ->
        @mat4.perspective cam.projection, props.fovy, props.aspect, props.near, props.far
        return
      'camera/position camera/rot camera/rotYMatrix':
        (cam, pos, rot, matY) ->
          m = cam.transform
          @mat4.fromTranslation m, pos
          @mat4.multiply m, m, matY
          @mat4.rotateX m, m, rot.x
          @mat4.invert m, m
          return


  # ===== Planes =====

  'planes/count':
    value:
      10


  'planes/fieldsize':
    value:
      x: [-100, 100]
      y: [-100, 100]
      z: [-100, 100]


  'planes/positions':
    init:
      require: 'planes/count planes/fieldsize'
      do: (count, size) ->
        for i in [0..count]
          [
            @math.randIntInRange size.x...
            @math.randIntInRange size.y...
            @math.randIntInRange size.z...
          ]

  'planes/rotations':
    init:
      require: 'planes/count'
      do: (count) ->
        for i in [0..count]
          x: Math.random() * Math.PI * 2
          y: Math.random() * Math.PI * 2


  'planes/matrices':
    init:
      require: 'planes/rotations planes/positions'
      do: (rots, poss) ->
        @zipWith rots, poss, (rot, pos) =>
          m = @mat4.create()
          @mat4.fromTranslation m, pos
          @mat4.rotateX m, m, rot.x
          @mat4.rotateY m, m, rot.y


  'plane/segments':
    value:
      segX: 10
      segY: 10


  'plane/geometry':
    init:
      require: 'plane/segments'
      do: (props) ->
        @plane props.segX, props.segY


  'plane/shader':
    value:
      vert: require '!raw!./shaders/plane-material.vert'
      frag: require '!raw!./shaders/plane-material.frag'
      attribs:
        'position': 'f 3'
        'uv': 'f 2'
      uniforms:
        'transform': 'm 4'
        'camera': 'm 4'
        'projection': 'm 4'
        'tex': 't'


  'planes/names':
    init:
      require: 'planes/count'
      do: (count) ->
        for i in [0..count]
          'plane' + i


  'planes/objects':
    init:
      require: 'planes/matrices planes/names'
      do: (mats, names) ->
        @zipWith mats, names, (mat, name) ->
          id: name
          geometry: 'planeGeometry'
          shader: 'planeShader'
          uniforms:
            'tex': 'myTex'
            'transform': mat

    reactions:
      'camera': (planes, cam) ->
        for plane in planes
          plane.uniforms['camera'] = cam.transform
          plane.uniforms['projection'] = cam.projection
        return


  'layers/planes':
    init:
      require: 'planes/names'
      do: (names) ->
        type: @consts.LayerType.RENDER
        objects: names


  # ===== Effect layers =====

  'myTex/layer':
    value:
      type: consts.LayerType.EFFECT
      shader: 'myShader'
      buffered: true
      width: 256
      height: 256
      minFilter: 'LINEAR'


  'myTex/shader':
    value:
      frag: require '!raw!./shaders/my-shader.frag'
      vert: assetLib.shaders._renderResult.vert
      attribs: assetLib.shaders._renderResult.attribs


  'darken/shader':
    value:
      frag: require '!raw!./shaders/darken.frag'
      vert: assetLib.shaders._renderResult.vert
      attribs: assetLib.shaders._renderResult.attribs
      uniforms:
        "source": 't'


  'darken/layer':
    value:
      type: consts.LayerType.EFFECT
      shader: 'darken'
      uniforms:
        'source': consts.SOURCE_LAYER


  # ===== Scene =====

  'settings/clearColor':
    value: [0.0, 0.0, 0.0, 1.0]


  'scene/settings':
    init:
      require: 'settings/clearColor settings/size'
      do: (cC, s) ->
        clearColor: cC
        width: s.w
        height: s.h


  'renderLayers':
    value:
      ['myTex', 'planeLayer']
      # ['myTex', 'planeLayer', 'darken']


  'scene':
    init:
      require: 'ctx scene/settings'
      do: (ctx, settings) ->
        @Renderer.initSettings ctx, settings
        @Renderer.updateSize ctx

    reactions:
      'darken/shader': updateShader 'darken'
      'plane/shader': updateShader 'planeShader'
      'myTex/shader': updateShader 'myShader'
      'myTex/layer': updateLayer 'myTex'
      'darken/layer': updateLayer 'darken'
      'layers/planes': updateLayer 'planeLayer'
      'plane/geometry': updateGeometry 'planeGeometry'
      'planes/objects': (scene, planes) ->
        for plane in planes
          @Renderer.updateObject scene, plane.id, plane
        return

      'renderLayers tpf': (scene, layers, tpf) ->
        if tpf
          @Renderer.renderLayers scene, layers
        return
