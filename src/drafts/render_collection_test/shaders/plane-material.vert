uniform mat4 camera;
uniform mat4 transform;
uniform mat4 projection;
attribute vec3 position;
attribute vec2 uv;
varying vec2 vUv;
void main() {
    vUv = uv;
    gl_Position = projection * camera * transform * vec4(position, 1.0);
    /* gl_Position = projection * transform * vec4(position, 1.0); */
}
