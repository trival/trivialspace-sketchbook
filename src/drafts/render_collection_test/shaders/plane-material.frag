uniform sampler2D tex;
varying vec2 vUv;
void main() {
    gl_FragColor = texture2D(tex, vUv);
    /* gl_FragColor = vec4(1.0, 0.0, 1.0, 1.0); */
}
