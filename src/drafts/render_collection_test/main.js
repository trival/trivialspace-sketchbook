var System = require('tvs-entity-system/lib/runtime'),
    ObjectLoader = require('tvs-entity-system/lib/loaders/object-spec'),
    Spec = require('tvs-entity-system/lib/spec-manager'),
    React = require('react'),
    EntityViewer = require('tvs-entity-editor'),
    scene = require('./scene'),
    context = require('./runtime-context')


let sys = window.sys = System.create(),
    spec = window.spec = Spec.create()

sys.setContext(context)
ObjectLoader.load(scene, spec)
spec.sendToRuntime(sys)

let editor = document.createElement('div')
editor.id = 'editor'
document.body.appendChild(editor)

let controller = window.controller = EntityViewer.Controller.create(spec, sys)

React.render(React.createElement(EntityViewer.FramedComponent, {
  controller
}), editor)

window.animateId = spec.getEntityByNameNamespace('startAnimate').id
window.animatorId = spec.getEntityByNameNamespace('animator').id
