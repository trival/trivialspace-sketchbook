uniform sampler2D source;
varying vec2 vUv;
void main() {
    vec4 color = texture2D(source, vUv);
    gl_FragColor = vec4(color.rgb - 0.3, 1.0);
}
