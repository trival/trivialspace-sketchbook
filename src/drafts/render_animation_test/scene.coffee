consts = require 'ts/render/constants'
renderer = require 'ts/render/context'
lib = require 'ts/render/asset-lib'
plane = require 'ts/geometry/plane'
mat = require 'gl-matrix/src/gl-matrix/mat4'
{updateShader, updateGeometry, updateObject, updateLayer} = require 'ts/systems/helpers/render-update-reactions'

ctx = require './ctx'


module.exports =
  'fps':
    value: 0

  'settings:clearColor':
    value: [0.0, 0.0, 0.0, 1.0]

  'scene:settings':
    require: 'settings:clearColor'
    init: (cC, w, h) ->
      renderer.updateSize ctx
      clearColor: cC
      width: ctx.settings.width
      height: ctx.settings.height


  'camera:properties':
    value:
      fovy: Math.PI / 3
      near: 0.1
      far: 1000

    reactions:
      'scene:settings': (props, settings) ->
        props.aspect = settings.width / settings.height
        return



  'camera:matrix':
    init: -> mat.create()
    reactions:
      'camera:properties': (m, props) ->
        console.log m, props
        mat.perspective m, props.fovy, props.aspect, props.near, props.far
        return

  'plane:position':
    value: [0, 0, -20]

  'plane:rotXspeed':
    value: 0.0001

  'plane:rotX':
    value: 0.2
    reactions:
      'fps plane:rotXspeed': (rotY, fps, speed) ->
        rotY + fps * speed

  'plane:rotYspeed':
    value: 0.0004

  'plane:rotY':
    value: 0.3
    reactions:
      'fps plane:rotYspeed': (rotY, fps, speed) ->
        rotY + fps * speed

  'plane:matrix':
    init: -> mat.create()
    reactions:
      'plane:position plane:rotX plane:rotY': (m, pos, rotX, rotY) ->
        mat.fromTranslation m, pos
        mat.rotateX m, m, rotX
        mat.rotateY m, m, rotY
        return

  'plane:segments':
    value:
      segX: 10
      segY: 10

  'plane:geometry':
    require: 'plane:segments'
    init: (props) ->
      plane props.segX, props.segY


  'plane:shader':
    value:
      vert: require '!raw!./shaders/plane-material.vert'
      frag: require '!raw!./shaders/plane-material.frag'
      attribs:
        'position': 'f 3'
        'uv': 'f 2'
      uniforms:
        'transform': 'm 4'
        'camera': 'm 4'
        'tex': 't'

  'objects:plane':
    value:
      geometry: 'planeGeometry'
      shader: 'planeShader'
      uniforms:
        'tex': 'myTex'

    reactions:
      'camera:matrix': (plane, cam) ->
        plane.uniforms['camera'] = cam
        return
      'plane:matrix': (plane, mat) ->
        plane.uniforms['transform'] = mat
        return


  'layers:plane':
    value:
      type: consts.LayerType.RENDER
      objects: ['plane']


  'myTex:layer':
    value:
      type: consts.LayerType.EFFECT
      shader: 'myShader'
      buffered: true
      width: 256
      height: 256
      minFilter: 'LINEAR'

  'myTex:shader':
    value:
      frag: require '!raw!./shaders/my-shader.frag'
      vert: lib.shaders._renderResult.vert
      attribs: lib.shaders._renderResult.attribs


  'darken:shader':
    value:
      frag: require '!raw!./shaders/darken.frag'
      vert: lib.shaders._renderResult.vert
      attribs: lib.shaders._renderResult.attribs
      uniforms:
        "source": 't'

  'darken:layer':
    value:
      type: consts.LayerType.EFFECT
      shader: 'darken'
      uniforms:
        'source': consts.SOURCE_LAYER

  'renderLayers':
    value: ['myTex', 'planeLayer', 'darken']

  'scene':
    require: 'scene:settings'
    init: (settings) ->
      renderer.initSettings ctx, settings
      renderer.updateSize ctx

    reactions:
      'darken:shader': updateShader 'darken'
      'plane:shader': updateShader 'planeShader'
      'myTex:shader': updateShader 'myShader'
      'myTex:layer': updateLayer 'myTex'
      'darken:layer': updateLayer 'darken'
      'layers:plane': updateLayer 'planeLayer'
      'plane:geometry': updateGeometry 'planeGeometry'
      'objects:plane': updateObject 'plane'

      'renderLayers fps': (scene, layers, fps) ->
        if fps
          renderer.renderLayers scene, layers
        return


