ES = require 'ts/systems/entity-system'
Animation = require 'ts/utils/animation'
scene = require './scene'
ctx = require './ctx'
sys = require './sys'


document.body.appendChild ctx.gl.canvas

ES.addEntities sys, scene

window.sys = sys
window.animator ?= Animation.animator (fps) ->
  ES.set sys, 'fps', fps
  ES.flush sys
  # console.log 'flush'


init = ->
  animator.start()


module.exports =
  init: init


if module.hot
  module.hot.accept './scene', ->
    scene = require './scene'
    init()
  module.hot.accept()
  module.hot.dispose ->
    console.log 'Disposing!!!'
    document.body.removeChild document.getElementsByTagName('canvas')[0]
