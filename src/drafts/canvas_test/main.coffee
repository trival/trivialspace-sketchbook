tex = require 'ts/graphics/textures'


canvas =
  tex.createTileNoiseTexture(
    window.innerWidth,
    window.innerHeight,
    4,4
  )

document.body.appendChild canvas

