consts = require 'ts/render/constants'
lib = require 'ts/render/asset-lib'
plane = require 'ts/geometry/plane'
mat = require 'gl-matrix/src/gl-matrix/mat4'


createViewMatrix = ->
  position = mat.create()
  mat.translate position, position, [0, 0, -20]
  mat.rotateX position, position, 0.2
  mat.rotateY position, position, 0.3
  perspective = mat.perspective mat.create(), Math.PI / 3, 1, 0.1, 1000
  mat.mul perspective, perspective, position


module.exports =

  settings:
    clearColor: [0.5, 1.0, 1.0, 1.0]


  objects:
    'plane':
      geometry: 'planeGeometry'
      shader: 'planeMaterial'
      uniforms:
        'texture': 'myTex'
        'viewMatrix': createViewMatrix()


  geometries:
    'planeGeometry': plane 10, 10


  layers:
    'myTex':
      type: consts.LayerType.EFFECT
      shader: 'myShader'
      buffered: true
      width: 256
      height: 256
      minFilter: 'LINEAR'

    'darken':
      type: consts.LayerType.EFFECT
      shader: 'darken'
      uniforms:
        'source': consts.SOURCE_LAYER

    'planeLayer':
      type: consts.LayerType.RENDER
      objects: ['plane']


  shaders:
    'planeMaterial':
      vert:
        """
          uniform mat4 viewMatrix;
          attribute vec3 position;
          attribute vec2 uv;
          varying vec2 vUv;
          void main() {
              vUv = uv;
              gl_Position = viewMatrix * vec4(position, 1.0);
          }
        """
      frag:
        """
          uniform sampler2D texture;
          varying vec2 vUv;
          void main() {
              gl_FragColor = texture2D(texture, vUv);
          }
        """
      attribs:
        'position': 'f 3'
        'uv': 'f 2'
      uniforms:
        'viewMatrix': 'm 4'
        'texture': 't'

    'myShader':
      frag:
        """
          varying vec2 vUv;
          void main() {
              gl_FragColor = vec4(vUv.x, vUv.y, 1.0, 1.0);
          }
        """
      vert: lib.shaders._renderResult.vert
      attribs: lib.shaders._renderResult.attribs

    'darken':
      frag:
        """
          uniform sampler2D source;
          varying vec2 vUv;
          void main() {
              vec4 color = texture2D(source, vUv);
              gl_FragColor = vec4(color.rgb - 0.3, 1.0);
          }
        """
      vert: lib.shaders._renderResult.vert
      attribs: lib.shaders._renderResult.attribs
      uniforms:
        "source": 't'
