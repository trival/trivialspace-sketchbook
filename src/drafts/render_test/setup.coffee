renderer = require 'ts/render/context'
scene = require './scene'
ctx = require './ctx'


document.body.appendChild ctx.gl.canvas
renderer.updateSize ctx


init = ->
  renderer.init ctx, scene
  renderer.renderLayers ctx, ['myTex', 'planeLayer', 'darken']
  # renderer.renderLayers ctx, ['myTex']
  # renderer.renderLayers ctx, ['darken']
  # console.log ctx


module.exports =
  init: init


if module.hot
  module.hot.accept './scene', ->
    scene = require './scene'
    init()
  module.hot.accept()
  module.hot.dispose ->
    console.log 'Disposing!!!'
    document.body.removeChild document.getElementsByTagName('canvas')[0]
